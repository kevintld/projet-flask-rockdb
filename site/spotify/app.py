from flask import Flask
import os.path
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_debugtoolbar import DebugToolbarExtension

app = Flask(__name__)

app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))

app.config["SECRET_KEY"]="Balkany"
app.config['DEBUG_TB_PROFILER_ENABLED'] = False #change this value to enable debug mod
app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False #change this value to enable debug mod

toolbar = DebugToolbarExtension(app)

login_manager = LoginManager(app)
login_manager.login_view = "login"

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///' + mkpath('../spotici.db?charset=utf8'))
db = SQLAlchemy(app)
