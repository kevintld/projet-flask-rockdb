import requests
import json
import random
import musicbrainzngs
from rich.progress import track
import random

DEBUG = False

""" API USAGE EXAMPLES
Return Artist details from artist name
search.php?s={Artist name}
Example - theaudiodb.com/api/v1/json/1/search.php?s=coldplay

Return all Album details from artist name
searchalbum.php?s={Artist name}
Example - theaudiodb.com/api/v1/json/1/searchalbum.php?s=daft_punk

Return single album details from artist + album name
searchalbum.php?s={Artist name}&a={Album name}
Example - theaudiodb.com/api/v1/json/1/searchalbum.php?s=daft_punk&a=Homework

Return single album details from album name
searchalbum.php?a={Album name}
Example - theaudiodb.com/api/v1/json/1/searchalbum.php?a=Homework

Return track details from artist/track name
searchtrack.php?s={Artist_Name}&t={Single_Name}
Example - theaudiodb.com/api/v1/json/1/searchtrack.php?s=coldplay&t=yellow

Return Discography for an Artist with Album names and year only
discography.php?s={Artist_Name}
Example - theaudiodb.com/api/v1/json/1/discography.php?s=coldplay
"""
# utiliser ca pour trouver la jaquette
# tu peut parser c'est du json
# un lien vers le site https://musicbrainz.org/release/b9bbd807-db07-479b-a92a-1f7d72847df8
# une release un une musique et une release_group est un album
# https://python-musicbrainzngs.readthedocs.io/en/v0.7.1/usage/
# https://python-musicbrainzngs.readthedocs.io/en/v0.7.1/api/#musicbrainzngs.get_image


# exemple de resultat : https://theaudiodb.com/api/v1/json/523532/searchalbum.php?s=oliver_tree

# guide vers l'api https://www.theaudiodb.com/api_guide.php

key = "523532"

request_url = "http://theaudiodb.com/api/v1/json/{}/searchalbum.php?s=".format(key)

artists = ["lorenzo", "orelsan", "joji", "magenta", "billy joel", "marshmello", "bts",
           "ui", "exo", "sia", "The Weeknd", "Post Malone", "jul", "Dua Lipa", "47ter", "Bakermat",
           "oliver tree", "lil nas x", "nekfeu", "mike posner", "major lazer", "avicii", "pnl",
           "billie eilish", "Michael Jackson", "Ed Sheeran", "Maroon 5", "Eminem", "Justin Bieber", "Drake",
           "rick astley", "Sam Smith", "Travis Scott",
           "Ariana Grande", "c2c", "deorro", "johnny hallyday", "XXXTentacion", "julien doré"]

user_agent_brainz = ["spotyfree", "lolfree", "sabisu.me"]  # change headers to avoid ban

user_agent_list = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 '
    'Safari/605.1.15',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 '
    'Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
]

data = []

musicbrainzngs.set_rate_limit(False)
musicbrainzngs.set_hostname("10.0.0.24:5959")


def debug(message):
    if DEBUG:
        print(message)


def fetch_data(url):
    user_agent = random.choice(user_agent_list)
    headers = {'User-Agent': user_agent}
    return json.loads(requests.get(url, headers=headers).text)


print("Nombre d'artists : " + str(len(artists)))
errors = []
for artist in track(artists, description="Fetching artist info on api"):
    search_artist = artist.replace(" ", "_").strip()
    url = request_url + search_artist

    response = fetch_data(url)
    if response == {'album': None}:
        errors.append({"artist": search_artist})
    else:
        data.append(response)
print("Errors : " + str(len(errors)))
if len(errors) >= 1:
    for e in errors:
        print(e["artist"], "not found")


def get_cover_by_brainz_if(album):
    musicbrainzngs.set_useragent(random.choice(user_agent_brainz), "1.1", "ddsfdfsdf.fr")
    release_id = album["strMusicBrainzID"]
    debug(release_id)
    try:
        data_from_brainz = musicbrainzngs.get_release_group_image_list(release_id)
        debug(data_from_brainz)
        if "images" in data_from_brainz:
            res = data_from_brainz["images"][0]["image"]
            return res if res != "" else "https://eu.ui-avatars.com/api/?name={}&size=200".format(album["strAlbum"])

        else:
            return None
    except musicbrainzngs.ResponseError as err:
        debug('Error fetching musicbrainzapi' + "for : " + str(release_id) + " " + str(album))
        if str(album["strAlbumThumb"]).startswith("https://www.theaudiodb.com/images/media/album"):
            print("Error so take the first cover ", album["strAlbumThumb"])
            return album["strAlbumThumb"]
        return "https://eu.ui-avatars.com/api/?name={}&size=200".format(album["strAlbum"])


def get_description(album):
    album_description_en, album_description_fr = None, None
    if "strDescriptionEN" in album:
        album_description_en = album["strDescriptionEN"]
        if album_description_en is None or album_description_en == "" or album_description_en == 'null':
            album_description_en = "Unknown"

    if "strDescriptionFR" in album:
        album_description_fr = album["strDescriptionFR"]
        if album_description_fr is None or album_description_fr == "" or album_description_fr == 'null':
            album_description_fr = "Unknown"

    return album_description_en, album_description_fr


def save_albums_to_json(text_data):
    with open('albums_data.json', 'w') as outfile:
        json.dump(text_data, outfile)


def get_album_of_artists(artists_list):
    out_data = {}
    for artist in track(artists_list, description="Download additional data..."):
        albums = artist["album"]
        for album in albums:
            album_name = album["strAlbum"]
            album_gender = album["strGenre"]
            album_description_en, album_description_fr = get_description(album)
            out_data[album_name] = []
            out_data[str(album_name)].append({
                "artist_id": album["idArtist"],
                "album_id": album["idAlbum"],
                "artist_name": album["strArtist"],
                "album_release": album["intYearReleased"],
                "album_gender": "Unknown" if album_gender == "" or album_gender is None else album["strGenre"],
                "album_brainz_id": album["strMusicBrainzID"],
                "album_brainz_artist_id": album["strMusicBrainzArtistID"],
                "album_cover": get_cover_by_brainz_if(album),
                "album_description_en": album_description_en,
                "album_description_fr": album_description_fr
            })
    return out_data


all_albums = get_album_of_artists(data)

save_albums_to_json(all_albums)
