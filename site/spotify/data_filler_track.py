import threading
import requests
import json
from youtubesearchpython import SearchVideos
from rich.progress import track
import random
import time

"""
https://theaudiodb.com/api/v1/json/1/track.php?m=2115888 (Example of track lists)

"""
DEBUG = True

key = "523532"

request_url = "https://theaudiodb.com/api/v1/json/{}/track.php?m=".format(key)

user_agent_list = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 '
    'Safari/605.1.15',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 '
    'Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
]

all_tracks = []

all_tracks_ready = []


def debug(message):
    if DEBUG:
        print(message)


def read_albums():
    with open('albums_data.json') as json_file:
        return json.load(json_file)


def fetch_data(url):
    user_agent = random.choice(user_agent_list)
    headers = {'User-Agent': user_agent}
    return json.loads(requests.get(url, headers=headers).text)


def get_tack_video(name):
    search = SearchVideos(name, offset=0, mode="json", max_results=1)

    res = json.loads(search.result())
    if search is None or len(res["search_result"]) == 0:
        print("No data founded")
        return

    if "link" in res["search_result"][0]:
        return res["search_result"][0]['link']
    else:
        print(res["search_result"][0])


def get_songs_from_albums():
    start_time = time.time()
    albums = read_albums()

    expanded = []
    for album in track(list(albums.values()), description="Fetching data"):
        album_id = album[0]["album_id"]
        res = fetch_data(request_url + str(album_id))

        for album_track in res["track"]:
            s = list()
            s.append(album)
            s.append(album_track)

            expanded.append(s)

    def run():
        while len(expanded):
            (album, aTrack) = expanded.pop(0)

            track_name = aTrack["strTrack"]

            album_infos = album[0]
            print(album_infos)
            all_tracks.append({
                "track_id": aTrack["idTrack"],
                "track_album_id": album_infos['album_id'],
                "track_name": aTrack["strTrack"],
                "track_id_artist": aTrack["idArtist"],
                "track_artist": aTrack["strArtist"],
                "track_duration": aTrack["intDuration"],
                "track_img": "Unknown",
                "track_gender": aTrack["strGenre"],
                "track_youtube": get_tack_video(track_name),
                "track_musicbrainz_id": aTrack["strMusicBrainzID"]
            })

            debug(f"Trying to search {track_name} ({len(expanded)})")

    threads = []
    for i in range(20):
        threads.append(t := threading.Thread(target=run))
        t.start()

    for t in threads:
        t.join()

    end_time = time.time()
    print("Time for fetching :", end_time - start_time)


def search_cover(data, album_id):
    for album in data:
        if data[album][0]['album_id'] == album_id:
            return data[album][0]['album_cover']


def fill_track_cover(track_list):
    data_albums = read_albums()

    for album in track_list:

        res = search_cover(data_albums, album["track_album_id"])
        cover = "Unknown" if res is None else res
        album['track_img'] = cover
        if album['track_gender'] is None:
            album['track_gender'] = "Unknown"


def save_albums_to_json(text_data):
    with open('tracks_data.json', 'w') as outfile:
        json.dump(text_data, outfile)


get_songs_from_albums()
fill_track_cover(all_tracks)
save_albums_to_json(all_tracks)
