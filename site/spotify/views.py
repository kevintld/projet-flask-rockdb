from __future__ import print_function

from .app import app
from flask import render_template, request, redirect, url_for, escape, session
from .models import *
from flask_login import login_required, login_user, logout_user, current_user
from wtforms import StringField, PasswordField, HiddenField
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from .models import User
from hashlib import sha256
from flask.helpers import flash
from .models import Utils
from deepdiff import DeepDiff
from pprint import pprint


@app.context_processor
def global_user():
    return dict(
        user=current_user,
        playlists=ORM.get_playlist_current_user()
    )


@app.route('/')
def index():
    return render_template(
        "index.html"
    )


@app.route('/test/', methods=['GET', 'POST'])
def test():
    return render_template(
        "test.html"
    )


@app.route('/home/', methods=['GET', 'POST'])
@login_required
def home():
    if '_user_id' in session:
        current_username = session['_user_id']

    else:
        current_username = None

    return render_template(
        "home.html",
        title="SPOTICI | Home",
        home="active",
        top_albums=ORM.get_top_5_album(),
        top_genres=ORM.get_top_5_genres(),
        top_artistes=ORM.get_top_5_artists()
    )


'''
--------------------------------- ALBUMS -------------------------------------------
'''


@app.route('/albums/details/<string:nom>/note', methods=['GET', 'POST'])
@login_required
def note(nom):
    if '_user_id' in session:
        current_username = session['_user_id']
    else:
        return redirect(url_for("erreur"))
    ORM.set_track_note(current_username, request.form["title"], request.form["score"], request.form["already_noted"])
    album = ORM.get_album(str(request.form['title']), str(request.form['artist']))
    tracks = ORM.get_tracks_by_album_id(album.album_id)
    already_note = ORM.is_note(current_username, album.album_name)
    return render_template(
        'details.html',
        res=album,
        album_tracks=tracks,
        selection="albums",
        username=current_username,
        note=already_note
    )


@app.route('/<string:selection>/delete/', methods=['GET', 'POST'])
@login_required
def nom_delete(selection):
    if selection in ['albums', 'artistes', 'musiques']:
        ORM.delete(selection)
        flash("Suppression effectuée !", "success")
        return redirect(url_for("home"))
    else:
        flash("La suppression a échoué !", "error")
        return redirect(url_for("erreur"))


@app.route('/album/details/<int:album_id>/', methods=['GET'])
@login_required
def album_details_by_album_id(album_id):
    tracks = ORM.get_tracks_by_album_id(album_id)
    res = ORM.get_album_by_id(album_id)

    if '_user_id' in session:
        current_username = session['_user_id']

    already_note = ORM.is_note(current_username, res[0].album_name)

    return render_template(
        'details.html',
        album_tracks=tracks,
        res=res[0],
        selection="albums",
        username=current_username,
        note=already_note
    )


@app.route('/<string:selection>/details/<string:nom>/edit/', methods=['GET', 'POST'])
@app.route('/<string:selection>/details/<string:nom>/', methods=['GET', 'POST'])
@login_required
def nom_details(selection, nom):
    if selection in ['albums', 'artistes', 'musiques']:
        res = ORM.get_criteres(selection)
    else:
        flash("Catégorie introuvable !", "error")
        return redirect(url_for("erreur"))

    if '_user_id' in session:
        current_username = session['_user_id']
    title = "SPOTICY | " + selection + " : " + nom

    if 'edit' in request.path:
        return render_template(
            "edit.html",
            res=res,
            selection=selection
        )
    # details for an album
    if selection == "albums":
        tracks = ORM.get_tracks_by_album_id(res.album_id)
        already_note = ORM.is_note(current_username, res.album_name)
        return render_template(
            'details.html',
            res=res,
            album_tracks=tracks,
            selection=selection,
            username=current_username,
            note=already_note
        )
    elif selection == "musiques":  # Song details

        return render_template(
            'details.html',
            res=res,
            duration=Utils.convert_millis(res.track_duration),
            selection=selection
        )
    elif selection == "artistes":  # Artists details
        return render_template(
            'details.html',
            res=res,
            selection=selection
        )
    else:
        return redirect(url_for("erreur"))


@app.route('/musiques/details/<int:id>/', methods=['GET'])
@login_required
def id_details(id):
    """
    Redirect to details by a given track id
    :param id:
    :return:

    """
    res = ORM.get_track(id)
    playlists = ORM.get_track_playlist_by_id(id)
    current_list_playlist = ""
    cleaned = []
    for playlist in playlists:
        cleaned.append("'" + str(playlist[0].playlist_id) + "'")
    current_list_playlist = ",".join(cleaned)

    return render_template(
        'details.html',
        res=res,
        playlist_selected=str(current_list_playlist),
        duration=Utils.convert_millis(res.track_duration),
        selection="musiques"
    )


@app.route('/<string:selection>/details/<string:nom>/edit/validate/', methods=['GET', 'POST'])
@login_required
def album_details_edit_validate(selection, nom):
    if selection == 'albums':
        album = ORM.edit_album(
            request.form['original_title'],
            request.form['original_artist']
        )
        flash("L'album " + str(request.form['title']) + " a bien été modifié.", "success")
        return redirect(url_for("nom_details", selection="albums", nom=album.album_name),
                        code=307)  # code 307 to keep post args

    elif selection == 'artistes':
        artiste = ORM.edit_artist(request.form['id'])
        flash("L'artiste " + str(escape(request.form['name'])) + " a bien été modifié.", "success")
        return redirect(url_for("nom_details",
                                selection="artistes",
                                nom=artiste.artist_name),
                        code=307)  # code 307 to keep post args

    elif selection == 'musiques':
        track = ORM.edit_track(request.form['id'])
        flash("La musique " + str(request.form['title']) + " a bien été modifiée.", "success")
        return redirect(url_for("nom_details",
                                selection="musiques",
                                nom=track.track_name),
                        code=307)  # code 307 to keep post args


@app.route('/play/<string:selection>/', methods=['GET', 'POST'])
@login_required
def play_song(selection):
    track_data = ORM.get_data_from_track(selection)
    youtube_id = "Unknown"
    if track_data.track_youtube is not None:
        youtube_id = track_data.track_youtube.split("=")[1]

    return render_template("song_player.html", track=track_data, youtube_id=youtube_id)


'''
--------------------------------- RECHERCHE -------------------------------------------
'''


@app.route('/recherche/<string:selection>/<int:numpage>/', methods=['GET'])
@login_required
def search(selection, numpage):
    limit = 40

    if selection == "albums":
        dico = {
            "albums": ORM.get_albums()
        }
    elif selection == "artistes":
        dico = {
            "artistes": ORM.get_artists()
        }

    elif selection == "genres":
        dico = {
            "genres": ORM.get_genre_vue(ORM.get_genders()),
        }

    elif selection == "musiques":
        dico = {
            "musiques": ORM.get_songs(),
        }

    try:
        pagination = ORM.get_pagination(dico[selection], limit)
        res = pagination[numpage]
        maxi = list(pagination.keys())[-1]

        if numpage != 1:
            previous_page = url_for("search", selection=selection, numpage=numpage - 1)
        else:
            previous_page = url_for("search", selection=selection, numpage=numpage)

        if numpage == maxi:
            next_page = url_for("search", selection=selection, numpage=numpage)
        else:
            next_page = url_for("search", selection=selection, numpage=numpage + 1)

    except:
        flash("Recherche introuvable !", 'error')
        return redirect(url_for("erreur"))

    return render_template(
        "recherche.html",
        selection=selection,
        resultat=sorted(res),
        numpage=numpage,
        previous=previous_page,
        next=next_page,
    )


@app.route('/recherche/<string:selection>/resultat/<int:numpage>/')
@login_required
def result_research(selection, numpage):
    if request.method == 'POST':
        search_res = str(escape(request.form['search']))
    else:
        search_res = str(escape(request.args.get('search')))

    limit = 40

    dico = {
        "albums": ORM.get_album_research(search_res),
        "artistes": ORM.get_artist_research(search_res),
        "genres": ORM.get_genre_vue(ORM.get_genre_research(search_res)),
        "musiques": ORM.get_song_research(search_res),
    }

    try:
        pagination = ORM.get_pagination(dico[selection], limit)
        res = pagination[numpage]
        maxi = list(pagination.keys())[-1]

        if numpage != 1:
            previous_page = url_for("result_research", selection=selection, numpage=numpage - 1)
        else:
            previous_page = url_for("result_research", selection=selection, numpage=numpage)

        if numpage == maxi:
            next_page = url_for("result_research", selection=selection, numpage=numpage)
        else:
            next_page = url_for("result_research", selection=selection, numpage=numpage + 1)

        return render_template(
            "recherche.html",
            resultat=res,
            selection=selection,
            numpage=numpage,
            previous=previous_page,
            next=next_page,
        )
    except:
        flash("Recherche introuvable !", "error")
        return redirect(url_for("erreur"))


'''
--------------------------------- PLAYLIST -------------------------------------------
'''


@app.route('/playlist/<int:id_playlist>', methods=['GET'])
@login_required
def playlist_details(id_playlist):
    songs = get_songs_in_playlist_by_id(id_playlist)
    total_duration = Utils.convert_millis(ORM.get_duration_of_songs(songs))
    for i in range(len(songs)):
        songs[i].track_duration = Utils.convert_millis(songs[i].track_duration)
    if '_user_id' in session:
        current_username = session['_user_id']
    return render_template(
        "playlist.html",
        title="SPOTICI",
        songs=songs,
        number=len(songs),
        duration=total_duration,
        selected_playlists=ORM.get_playlist_by_id(id_playlist),
        current_username=current_username,
        playlist_creator_name=ORM.get_username_from_playlist_id(id_playlist),
        edit=False
    )


@app.route('/playlist/<int:id_playlist>/edit', methods=['GET'])
@login_required
def playlist_edit(id_playlist):
    if '_user_id' in session:
        current_username = session['_user_id']
    playlist_creator_name = ORM.get_username_from_playlist_id(id_playlist)
    if playlist_creator_name == current_username:
        songs = get_songs_in_playlist_by_id(id_playlist)
        total_duration = Utils.convert_millis(ORM.get_duration_of_songs(songs))
        for i in range(len(songs)):
            songs[i].track_duration = Utils.convert_millis(songs[i].track_duration)
        return render_template(
            "playlist.html",
            title="SPOTICI",
            songs=songs,
            number=len(songs),
            duration=total_duration,
            selected_playlists=ORM.get_playlist_by_id(id_playlist),
            current_username=current_username,
            playlist_creator_name=ORM.get_username_from_playlist_id(id_playlist),
            edit=True
        )
    else:
        return redirect(url_for("erreur"))


@app.route('/playlist/<int:id_playlist>/<int:id_track>/delete', methods=['GET'])
@login_required
def playlist_delete_track(id_playlist, id_track):
    if '_user_id' in session:
        current_username = session['_user_id']
    playlist_creator_name = ORM.get_username_from_playlist_id(id_playlist)
    if playlist_creator_name == current_username:
        if ORM.is_track_from_playlist(id_playlist, id_track):
            ORM.remove_track_from_playlist(id_playlist, id_track)
            return redirect(url_for("playlist_edit", id_playlist=id_playlist))
    return redirect(url_for("erreur"))


@app.route('/playlist/<int:id_playlist>/nom', methods=['GET', "POST"])
@login_required
def playlist_new_name(id_playlist):
    if '_user_id' in session:
        current_username = session['_user_id']
    playlist_creator_name = ORM.get_username_from_playlist_id(id_playlist)
    if playlist_creator_name == current_username:
        ORM.change_playlist_name(id_playlist, request.form["playlist_new_name"])
        return redirect(url_for("playlist_edit", id_playlist=id_playlist))
    return redirect(url_for("erreur"))


@app.route('/playlist/add/<string:username>', methods=['POST'])
@login_required
def playlist_add(username):
    if request.method == "POST":
        playlist_name = str(escape(request.form["title"]))
        playlist_image = str(escape(request.form["image"]))

        if playlist_name is not None and playlist_image is not None:
            ORM.create_playlist(username, playlist_name, playlist_image)
            flash("Playlist créée avec succès !", "success")
            print(username, " new playlist : ", playlist_name, ' img : ', playlist_image)
        else:
            flash("La création de la playlist a échoué !", "error")
            return redirect(url_for("erreur"))
        return redirect(url_for("home"))
    else:
        return redirect(url_for("erreur"))


@app.route('/playlist/update/song/<int:song_id>', methods=['POST'])
@login_required
def playlist_update(song_id):
    if request.method == "POST":
        playlists_selected = str(escape(request.form["playlists"]))
        playlist_user = str(escape(request.form["userplaylist"]))

        if playlists_selected is not None and playlist_user is not None:

            current_song_playlist = [str(x[0].playlist_id) for x in ORM.get_track_playlist_by_id(song_id)]
            selected_items = playlists_selected.split(",")
            while True:
                try:
                    selected_items.remove('')
                except ValueError:
                    break
            print("=" * 50)
            print("Song edited : ", song_id)
            print("request ", ORM.get_track_playlist_by_id(song_id))
            print("Current list : ", current_song_playlist)
            print("New list : ", selected_items)
            ddiff = DeepDiff(current_song_playlist, list(selected_items))
            pprint(ddiff)
            print("=" * 50)

            if "iterable_item_added" in ddiff.keys():
                print("On a ajouter des truc ")
                added = ddiff["iterable_item_added"]
                for u, x in added.items():
                    ORM.add_track_from_playlist(x, song_id)

            if "iterable_item_removed" in ddiff.keys():
                print("On a supprimer des truc ")
                removed = ddiff["iterable_item_removed"]
                for u, x in removed.items():
                    ORM.remove_track_from_playlist(x, song_id)

            if "values_changed" in ddiff.keys():
                print("Des trucs on etes modifiés")
                updated = ddiff["values_changed"]
                for u, x in updated.items():
                    ORM.update_track_from_playlist(int(x['old_value']), int(x['new_value']), song_id)

            flash("Modifications effectuées !", "success")


        else:
            flash("L'ajout de la musique à la playlist a échoué !", "error")
            return redirect(url_for("erreur"))
        return redirect(url_for("home"))
    else:
        return redirect(url_for("erreur"))


@app.route('/allPlaylists/<string:username>', methods=['GET'])
@login_required
def get_all_playlists_by_username(username):
    """
    return all playlist created by user
    :param username:
    :return:
    """
    return render_template(
        "allPlaylists.html",
        title="SPOTICI",
        allPlaylists="active",
        playlists=ORM.get_playlist_by_user(username)
    )


@app.route('/playlist/<int:id_playlist>/delete', methods=['GET'])
@login_required
def playlist_delete_id(id_playlist):
    if '_user_id' in session:
        current_username = session['_user_id']
        if current_username == ORM.get_username_from_playlist_id(id_playlist):
            flash("Suppression de la playlist \"" + str(
                ORM.get_playlist_name_from_playlist_id(id_playlist)) + "\" effectuée !", "success")
            ORM.delete_playlist_by_id(id_playlist)
            return redirect(url_for("home"))
    flash(
        "La suppression de la playlist \"" + str(ORM.get_playlist_name_from_playlist_id(id_playlist)) + "\" a échoué !",
        "error")
    return redirect(url_for("erreur"))


'''
--------------------------------- LOGIN -------------------------------------------
'''


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_data(self):
        return self.username.data

    def get_authenticated_user(self):
        user = ORM.load_user(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.user_password else None


@app.route('/connexion/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if not form.is_submitted():
        form.next.data = request.args.get("next")
    elif form.validate_on_submit():
        user = form.get_authenticated_user()
        if user:
            login_user(user)
            next_form = form.next.data or url_for("home")
            return redirect(next_form)
        else:
            flash("La combinaison saisie est incorrecte !", "error")
    return render_template(
        "connexion.html",
        form=form,
    )


class RegisterForm(FlaskForm):
    id = HiddenField("id")
    name = StringField("Nom", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    confirm_password = PasswordField("ConfirmPassword", validators=[DataRequired()])


@app.route('/inscription/', methods=['GET', 'POST'])
def register():
    return render_template(
        "inscription.html",
    )


@app.route('/inscription/validation/', methods=['GET', 'POST'])
def register_validate():
    # TODO move this into models
    username = str(escape(request.form['username']))
    email = str(escape(request.form['email']))
    password = str(escape(request.form['password']))
    confirm_password = str(escape(request.form['confirmpassword']))
    print(password == confirm_password)
    print(ORM.is_username_available(username))
    if password == confirm_password and ORM.is_username_available(username):
        m = sha256()
        m.update(password.encode())
        u = User(
            user_username=username,
            user_password=m.hexdigest(),
            user_img="https://eu.ui-avatars.com/api/?name={}".format(username),
            user_email=email,
            is_admin_user=False
        )
        db.session.add(u)
        db.session.commit()

        return redirect(url_for('login'))
    else:
        flash("Nom d'utilisateur indisponible ou mots de passes différents !", "error")
        return redirect(url_for('register'))


'''
--------------------------------- CREATE -------------------------------------------
'''


@app.route('/creation/<string:selection>', methods=['GET'])
@login_required
def create(selection):
    if selection == 'albums':
        return render_template("create/create-album.html")
    elif selection == 'artistes':
        return render_template("create/create-artist.html")
    elif selection == 'musiques':
        return render_template("create/create-track.html")
    else:
        flash("Catégorie inconnue !", "error")
        return redirect(url_for("erreur"))


@app.route('/creation/<string:selection>/validation/', methods=['POST'])
@login_required
def validate_creation(selection):
    if selection == "musiques":
        if not ORM.is_artist_present(request.form["artist_name"]):
            return render_template("create/validate/validate-creation.html", erreur="non_existent_artist")

        if not ORM.is_album_present(request.form["album_name"]):
            return render_template("create/validate/validate-creation.html", erreur="non_existent_album")

        if ORM.is_track_name_already_in_album(request.form["album_name"], request.form["track_name"]):
            return render_template("create/validate/validate-creation.html", erreur="name_already_in_album")

        if not ORM.is_artist_creator_of_album(request.form["album_name"], request.form["artist_name"]):
            return render_template("create/validate/validate-creation.html", erreur="artist_not_creator")

        else:
            ORM.add_new_track(request.form)

    elif selection == "artistes":
        if ORM.is_artist_present(request.form["artist_name"]):
            return render_template("create/validate/validate-creation.html", erreur="artist_already_present")
        else:
            ORM.add_new_artist(request.form)

    elif selection == "albums":
        if ORM.is_creator_album_present(request.form["artist_name"], request.form["album_name"]):
            return render_template("create/validate/validate-creation.html", erreur="album_already_existing")
        else:
            ORM.add_new_album(request.form)
    else:
        flash("Catégorie inconnue !", "error")
        return redirect(url_for("error"))
    # TODO redirect to the newly created track please if possible
    return redirect(url_for("home"))


'''
--------------------------------- OTHERS -------------------------------------------
'''


@app.route('/tests', methods=['GET', 'POST'])
@login_required
def tests():
    return render_template(
        "tests.html",
    )


@app.route("/logout/")
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/error/')
def erreur():
    return render_template('404.html')


# TODO Delete this
@app.route("/testing")
def testing():
    return render_template("create/CreateAlbum.html")
