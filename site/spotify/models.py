from .app import db
from flask_login import UserMixin, current_user
from .app import login_manager
from sqlalchemy import PrimaryKeyConstraint, ForeignKey, join, desc, func, cast, Integer
from flask import request, escape
from sqlalchemy import asc, desc
from sqlalchemy.orm import relationship
import json


class Artist(db.Model):
    __tablename__ = 'artist'
    artist_id = db.Column(db.String(120), primary_key=True)
    artist_name = db.Column(db.String(120))
    artist_img = db.Column(db.String(120))

    albums = relationship("Album", back_populates="artists", cascade="delete, merge, save-update")

    def __lt__(self, other):
        return self.artist_name < other.artist_name

    def __repr__(self):
        return "artist_name={0}".format(self.artist_name)


class Album(db.Model):
    __tablename__ = 'album'
    __table_args__ = (
        PrimaryKeyConstraint('album_name', 'album_artist', 'album_id'),
    )
    album_name = db.Column(db.String(120))
    album_artist_id = db.Column(db.String(120))
    album_id = db.Column(db.String(120))
    album_artist = db.Column(db.String(120), ForeignKey("artist.artist_name"))
    album_release_date = db.Column(db.String(4))
    album_cover_img = db.Column(db.String(2000))
    album_description_en = db.Column(db.Text(10000))
    album_description_fr = db.Column(db.Text(10000))
    album_brainz_id = db.Column(db.String(100))
    album_brainz_artist_id = db.Column(db.String(100))
    tracks = relationship("Track", back_populates="albums", cascade="delete, merge, save-update")
    notes = relationship("Score", back_populates="albums", cascade="delete, merge, save-update")
    artists = relationship("Artist", back_populates="albums")

    def __lt__(self, other):
        return self.album_name < other.album_name


class Track(db.Model):
    __tablename__ = 'track'
    track_id = db.Column(db.String(120), primary_key=True)
    track_name = db.Column(db.String(120))
    track_id_artist = db.Column(db.String(120))
    track_id_album = db.Column(db.String(120), ForeignKey("album.album_id"))
    track_artist = db.Column(db.String(120))
    track_duration = db.Column(db.Integer())
    track_img = db.Column(db.String(2000))
    track_gender = db.Column(db.String(50))
    track_youtube = db.Column(db.String(2000))
    track_musicbrainz_id = db.Column(db.String(400))
    albums = relationship("Album", back_populates="tracks")

    def __repr__(self):
        return "track_name={0}".format(self.track_name)

    def __lt__(self, other):
        return self.track_name < other.track_name


class Playlist(db.Model):
    __tablename__ = 'playlist'
    playlist_id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    playlist_title = db.Column(db.String(120), nullable=False)
    playlist_cover = db.Column(db.String(2000), nullable=False)
    playlist_username = db.Column(db.String(120), nullable=False)

    compose = relationship("ComposePlaylistTrack", back_populates="playlists", cascade="delete, merge, save-update")

    def __repr__(self):
        return "playlist_id={0}," \
               " playlist_title={1}," \
               " playlist_cover={2}," \
               "playlist_username={3}".format(self.playlist_id,
                                              self.playlist_title,
                                              self.playlist_cover,
                                              self.playlist_username)


class ComposePlaylistTrack(db.Model):
    __tablename__ = 'compose_playlist'
    __table_args__ = (
        PrimaryKeyConstraint('compose_playlist_track_id'),
    )
    compose_playlist_track_id = db.Column(db.Integer(),autoincrement=True, nullable=False)
    compose_playlist_track_playlist_id = db.Column(db.Integer(), ForeignKey("playlist.playlist_id"), nullable=False)
    compose_playlist_track_track_id = db.Column(db.Integer() , nullable=False)

    playlists = relationship("Playlist", back_populates="compose")

    def __repr__(self):
        return "compose_playlist_track_playlist_id={0},compose_playlist_track_track_id={1}".format(
            self.compose_playlist_track_playlist_id, self.compose_playlist_track_track_id)


class Score(db.Model):
    __tablename__ = 'score'
    __table_args__ = (
        PrimaryKeyConstraint('score_username', 'score_album'),
    )
    score_album = db.Column(db.String(120), ForeignKey("album.album_name"), nullable=False)
    score_username = db.Column(db.String(120), nullable=False)
    score_score = db.Column(db.Integer(), nullable=False)

    albums = relationship("Album", back_populates="notes")

    def __repr__(self):
        return "score_album={0},score_username={1},score_username={2}".format(self.score_album, self.score_username,
                                                                              self.score_score)


class User(db.Model, UserMixin):
    user_username = db.Column(db.String(120), primary_key=True)
    user_password = db.Column(db.String(120))
    user_img = db.Column(db.String(2000))
    user_email = db.Column(db.String(120))
    is_admin_user = db.Column(db.Boolean)

    def get_id(self):
        return self.user_username

    def get_user_img(self):
        return self.user_img


class Utils:
    @staticmethod
    def convert_millis(milliseconds):
        seconds, milliseconds = divmod(milliseconds, 1000)
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)
        seconds = seconds + milliseconds / 1000
        return "{0}:{1}".format(minutes, int(seconds))


class ORM:

    @staticmethod
    def get_album_by_id(album_id):
        return db.session.query(Album).filter(Album.album_id == album_id).all()

    @staticmethod
    def get_track_playlist_by_id(id):
        query_res = db.session\
            .query(Playlist, ComposePlaylistTrack)\
            .join(Playlist,  Playlist.playlist_id == ComposePlaylistTrack.compose_playlist_track_playlist_id, isouter=True)\
            .filter(ComposePlaylistTrack.compose_playlist_track_track_id == id)\
            .all()

        print(query_res)

        return query_res

    @staticmethod
    def create_playlist(username, playlist_name, playlist_image):
        db.session.add(
            Playlist(playlist_username=username, playlist_cover=playlist_image, playlist_title=playlist_name))
        db.session.commit()

    @staticmethod
    def get_duration_of_songs(songs):
        total = 0
        for song in songs:
            total += song.track_duration
        return total

    @staticmethod
    @login_manager.user_loader
    def load_user(username):
        return User.query.filter(User.user_username == username).first()

    @staticmethod
    def get_tracks_by_album_id(album_id):
        """
        Get all track by album_id
        :param album_id:
        :return: list of tracks
        """
        res = Track.query.filter(Track.track_id_album == album_id).distinct().all()
        for i in range(len(res)):
            new_duration = Utils.convert_millis(res[i].track_duration)
            res[i].track_duration = new_duration
        return res

    @staticmethod
    def get_playlist_by_user(username):
        if username is not None:
            res = Playlist.query.filter(Playlist.playlist_username == username).all()
            return res

    @staticmethod
    def get_playlist_current_user():
        username = None
        if current_user.is_authenticated:
            username = current_user.user_username

        if username is not None:
            res = Playlist.query.filter(Playlist.playlist_username == username).all()
            return res

    @staticmethod
    def get_playlist_by_id(id_playlist):
        return Playlist.query.filter(Playlist.playlist_id == id_playlist).all()

    @staticmethod
    def search_playlist_by_id(id_playlist):
        return db.session.Playlist.query.filter(Playlist.playlist_id.like("%" + id_playlist + "%"))

    @staticmethod
    def get_albums():
        return Album.query.all()

    @staticmethod
    def get_users():
        return User.query.all()

    @staticmethod
    def get_artists():
        return Artist.query.all()

    @staticmethod
    def delete_playlist_by_id(playlist_id):
        db.session.delete(ORM.get_playlist_by_id(playlist_id)[0])
        db.session.commit()

    @staticmethod
    def get_songs():
        res = Track.query.order_by(desc(Track.track_name)).all()
        return res

    @staticmethod
    def get_genders():
        return db.session.query(Track.track_gender).distinct().all()

    @staticmethod
    def get_albums_limit(n):
        return Album.query.limit(n).all()

    @staticmethod
    def get_artists_limit(n):
        return Artist.query.limit(n).all()

    @staticmethod
    def get_gender_limit(n):
        return db.session.query(Track.track_gender).distinct().limit(n).all()

    @staticmethod
    def get_song_limit(n):
        return Track.query.filter(Track.track_name).limit(n).all()

    @staticmethod
    def get_album_research(word):
        return Album.query.filter(Album.album_name.like("%" + str(word) + "%")).all()

    @staticmethod
    def get_album_research_limit(word, n):
        return Album.query.filter(Album.album_name.like("%" + str(word) + "%")).limit(n).all()

    @staticmethod
    def get_artist_research(word):
        return Artist.query.filter(Artist.artist_name.like("%" + str(word) + "%")).all()

    @staticmethod
    def get_artist_research_limit(word, n):
        return Artist.query.filter(Artist.artist_name.like("%" + str(word) + "%")).limit(n).all()

    @staticmethod
    def get_genre_research(word):
        return db.session.query(Track.track_gender).distinct().filter(
            Track.track_gender.like("%" + str(word) + "%")).all()

    @staticmethod
    def get_genre_research_limit(word, n):
        return db.session.query(Track.track_gender).distinct().filter(
            Track.track_gender.like("%" + str(word) + "%")).limit(n).all()

    @staticmethod
    def get_song_research(word):
        return Track.query.filter(Track.track_name.like("%" + str(word) + "%")).all()

    @staticmethod
    def get_song_research_limit(word, n):
        return Track.query \
            .filter(Track.track_name.like("%" + str(word) + "%")) \
            .limit(n) \
            .all()

    @staticmethod
    def get_album(title, artist):
        return db.session.query(Album).filter(
            Album.album_name.like(title),
            Album.album_artist.like(artist)
        ).first()

    @staticmethod
    def delete_album(title, artist):
        # remove from tracks , playlist , note
        db.session.delete(ORM.get_album(title, artist))
        db.session.commit()

    @staticmethod
    def delete_artist(index):
        # remove his album and tracks , note
        db.session.delete(ORM.get_artist(index))
        db.session.commit()

    @staticmethod
    def delete_track(index):
        db.session.delete(ORM.get_track(index))
        db.session.commit()

    @staticmethod
    def delete(selection):
        if selection == 'albums':
            title = str(request.form['title'])
            artist = str(request.form['artist'])

            ORM.delete_album(title, artist)

        elif selection == 'artistes':
            id = int(request.form['id'])
            ORM.delete_artist(id)

        elif selection == 'musiques':
            id = int(request.form['id'])
            ORM.delete_track(id)

    @staticmethod
    def get_top_5_artists():
        """
        Function that return the five best artists according to their albums scores
        :return:
        """

        return db.session.query(Artist, (func.sum(Score.score_score) / func.count(Score.score_album)) \
                                .label("score_tot")) \
            .join(Album, Artist.artist_name == Album.album_artist) \
            .join(Score, Album.album_name == Score.score_album) \
            .group_by(Artist.artist_name) \
            .order_by(desc("score_tot")) \
            .limit(5) \
            .all()

    @staticmethod
    def get_top_5_album():
        """
        Function that return a list of the best songs
        :return: List of the 5 best song in the database
        """
        return db.session.query(Album, (func.sum(Score.score_score) / func.count(Score.score_album)) \
                                .label("score_tot")) \
            .join(Score, Album.album_name == Score.score_album) \
            .group_by(Album.album_name) \
            .having(func.sum(Score.score_score)) \
            .order_by(desc("score_tot")) \
            .limit(5) \
            .all()

    @staticmethod
    def get_top_5_genres():
        """
        Function that return a list of the best songs
        :return: List of the 5 best song in the database
        """
        return db.session.query(Track.track_gender, func.count(Score.score_album).label("nombre")) \
            .group_by(Track.track_gender) \
            .order_by(desc("nombre")) \
            .limit(5) \
            .all()

    @staticmethod
    def get_pagination(liste, limite):
        if len(liste) == 0:
            return
        index = 1
        dico = {index: {liste[0]}}
        for i in range(1, len(liste)):
            if i % limite == 0:
                index += 1
                dico[index] = set()

            dico[index].add(liste[i])

        return dico

    @staticmethod
    def get_genre_vue(liste):
        res = []
        for elem in liste:
            parse = ''.join(elem)
            if parse != '':
                res.append(parse)
        return res

    @staticmethod
    def get_criteres(selection):
        if selection == 'albums':
            album = ORM.get_album(str(request.form['title']), str(request.form['artist']))

            return album

        elif selection == "artistes":
            id = int(escape(request.form['id']))
            artiste = ORM.get_artist(id)

            return artiste

        elif selection == "musiques":
            id = int(escape(request.form['id']))
            track = ORM.get_track(id)

            return track

    @staticmethod
    def get_artist(id):
        return Artist.query.get(id)

    @staticmethod
    def get_track(id):
        return Track.query.get(id)

    @staticmethod
    def edit_track(index):
        track = ORM.get_track(
            index
        )

        track.track_name = str(request.form['title'])
        track.track_artist = str(escape(request.form['artist']))
        track.track_duration = minutes_to_duration(request.form['duration'])
        track.track_gender = str(escape(request.form['gender']))
        track.track_img = str(escape(request.form['img']))
        db.session.commit()

        return track

    @staticmethod
    def edit_artist(index):
        artiste = ORM.get_artist(
            index
        )

        artiste.artist_name = str(escape(request.form['name']))
        artiste.artist_img = str(escape(request.form['img']))
        db.session.commit()

        return artiste

    @staticmethod
    def edit_album(title, artist):
        album = ORM.get_album(
            title, artist
        )

        album.album_name = str(request.form['title'])
        album.album_artist = str(escape(request.form['artist']))
        album.album_description_fr = str(escape(request.form['description']))
        album.album_release_date = str(escape(request.form['date']))
        album.album_cover_img = str(escape(request.form['img']))
        db.session.commit()

        return album

    @staticmethod
    def get_data_from_track(selection):
        return Track.query.filter(Track.track_id == selection).first()

    @staticmethod
    def is_note(current_username, album_name):
        """
        Function that return a boolean if the user already gave a note to the track and if the user gave a note it returns it
        :param current_username:
        :param album_name:
        :return: True or False
        """
        query = Score.query.filter(Score.score_album == album_name, Score.score_username == current_username).first()
        return (False, 5) if query is None else (True, query.score_score)

    @staticmethod
    def set_track_note(current_username, title, note, already_noted):
        """
        function that update the score or add a new one
        :param current_username:
        :param title:
        :param note:
        :param already_noted:
        :return:
        """
        if already_noted == "False":
            db.session.add(Score(score_album=title, score_username=current_username, score_score=note))
            db.session.commit()
        else:
            score = Score.query.filter(Score.score_album == title, Score.score_username == current_username).first()
            score.score_score = note
            db.session.commit()

    @staticmethod
    def is_username_available(pseudo):
        username = db.session.query(User).filter(User.user_username == pseudo).first()
        db.session.commit()
        return True if username is None else False

    @staticmethod
    def is_artist_present(artist_name):
        """
        Function that return true if the artist is present in the database
        :param artist_name:
        :return:
        """
        return True if Artist.query.filter(Artist.artist_name == artist_name).first() is not None else False

    @staticmethod
    def is_album_present(album_name):
        """
        Function that return true if the album is present in the database
        :param album_name:
        :return:
        """
        return True if Album.query.filter(Album.album_name == album_name).first() is not None else False

    @staticmethod
    def is_track_name_already_in_album(album_name, track_name):
        """
        Function that return true if the album already contains the track
        :param album_name:
        :param track_name:
        :return:
        """
        return True if Track.query.filter(Track.track_name == track_name, Album.album_name == album_name).join(Album, Track.track_id_album == Album.album_id).first() is not None else False

    @staticmethod
    def is_artist_creator_of_album(album_name, artist_name):
        """
        Function that return true if the artist is really the creator of the album
        :param album_name:
        :param artist_name:
        :return:
        """
        return True if Album.query.filter(Album.album_name == album_name, Album.album_artist == artist_name).first() is not None else False

    @staticmethod
    def add_new_track(form):
        """
        Function that add the newly created track to the database
        :param form:
        :return:
        """
        db.session.add(Track(track_id=str(int(ORM.get_max_id_track())+1),
                             track_name=form["track_name"],
                             track_id_artist=ORM.get_artist_id(form["artist_name"]),
                             track_id_album=ORM.get_album(form["album_name"], form["artist_name"]).album_id,
                             track_artist=form["artist_name"],
                             track_duration=ORM.time_to_milis(form["track_duration"]),#TODO make this again
                             track_img=form["img_link"],
                             track_gender=form["track_gender"],
                             track_youtube=form["yt_link"],
                             track_musicbrainz_id="Unknown"
                             ))
        db.session.commit()

    @staticmethod
    def get_max_id_track():
        """
        Function that return the max track id in the database (due to the API giving "random" like ids)
        :return:
        """
        return Track.query.filter(Track.track_id).order_by(desc(Track.track_id)).first().track_id

    @staticmethod
    def get_artist_id(artist_name):
        """
        Function that return the artist id
        :param artist_name:
        :return:
        """
        return Artist.query.filter(Artist.artist_name == artist_name).first().artist_id

    @staticmethod
    def add_new_artist(form):
        """
        function that add the artist to the database
        :param form:
        :return:
        """
        db.session.add(Artist(artist_name=form["artist_name"],
                              artist_id=str(int(ORM.get_max_id_artist())+1),
                              artist_img=form["img_link"]
                              ))
        db.session.commit()

    @staticmethod
    def get_max_id_artist():
        """
        Function that return the max artist id in the database (due to the API giving "random" like ids)
        :return:
        """
        return Artist.query.filter(Artist.artist_id).order_by(desc(Artist.artist_id)).first().artist_id

    @staticmethod
    def is_creator_album_present(artist_name, album_name):
        """
        Function that return true if the album created by a certain artist is already present
        :param artist_name:
        :param album_name:
        :return:
        """
        return Album.query.filter(Album.album_name == album_name, Album.album_artist == artist_name).first() is not None

    @staticmethod
    def add_new_album(form):
        """
        function that add the newly created album
        :param form:
        :return:
        """
        db.session.add(Album(album_name=str(form["album_name"]),
                             album_artist_id=str(ORM.get_artist_id(form["artist_name"])),
                             album_id=str(int(ORM.get_max_id_album())+1),
                             album_artist=str(form["artist_name"]),
                             album_release_date=str(form["album_year"]),
                             album_cover_img=str(form["img_link"]),
                             album_description_en=str("Unknown"),
                             album_description_fr=str(form["album_description"]),
                             album_brainz_id=str("Unknown"),
                             album_brainz_artist_id=str("Unknown")))

        db.session.commit()

    @staticmethod
    def get_max_id_album():
        """
        Function that return the max album id in the database (due to the API giving "random" like ids)
        :return:
        """
        return Album.query.filter(Album.album_id).order_by(desc(Album.album_id)).first().album_id

    @staticmethod
    def time_to_milis(time):
        """
        function that return the time in milisecond
        :param time:
        :return:
        """
        minu, sec = time.split(":")
        milis = int(minu) * 60 * 1000 + int(sec) * 1000
        print(milis)
        return str(milis)

    @staticmethod
    def get_username_from_playlist_id(playlist_id):
        """
        function that return the username that correspond to the playlist id
        :param playlist_id:
        :return:
        """
        username = Playlist.query.filter(Playlist.playlist_id == playlist_id).first().playlist_username
        return username

    @staticmethod
    def get_playlist_name_from_playlist_id(playlist_id):
        """
        function that return the playlist name that correspond to the playlist id
        :param playlist_id:
        :return:
        """
        name = Playlist.query.filter(Playlist.playlist_id == playlist_id).first().playlist_title
        return name

    @staticmethod
    def is_track_from_playlist(id_playlist, id_track):
        """
        Function that return true if the track is in the selected playlist
        :param id_playlist:
        :param id_track:
        :return:
        """
        return Playlist.query.filter(Playlist.playlist_id == id_playlist, ComposePlaylistTrack.compose_playlist_track_track_id == id_track).join(ComposePlaylistTrack, Playlist.playlist_id == ComposePlaylistTrack.compose_playlist_track_playlist_id).first() is not None


    @staticmethod
    def remove_track_from_playlist(id_playlist, id_track):
        """
        Function that remove the track from the selected playlist return error if the track is not in the album
        :param id_playlist:
        :param id_track:
        :return:
        """
        db.session.delete(ComposePlaylistTrack.query.filter(Playlist.playlist_id == id_playlist, ComposePlaylistTrack.compose_playlist_track_track_id == id_track).join(Playlist, Playlist.playlist_id == ComposePlaylistTrack.compose_playlist_track_playlist_id).first())
        db.session.commit()

    @staticmethod
    def add_track_from_playlist(id_playlist, id_track):
        """
        Function that add the track from the selected playlist
        :param id_playlist:
        :param id_track:
        :return:
        """
        db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=id_playlist, compose_playlist_track_track_id=id_track))
        db.session.commit()

    @staticmethod
    def update_track_from_playlist(old_id_playlist,new_id_playlist, id_track):
        """
        Function that update the track playlist from old id to new playlist
        :param id_playlist:
        :param id_track:
        :return:
        """
        db.session.query(ComposePlaylistTrack).filter(ComposePlaylistTrack.compose_playlist_track_track_id == id_track and ComposePlaylistTrack.compose_playlist_track_playlist_id==old_id_playlist).update({'compose_playlist_track_playlist_id': new_id_playlist})
        db.session.commit()

    @staticmethod
    def change_playlist_name(id_playlist, playlist_new_name):
        """
        Function that update the playlist name
        :param id_playlist:
        :param playlist_new_name:
        :return:
        """
        playlist = Playlist.query.filter(Playlist.playlist_id == id_playlist).first()
        playlist.playlist_title = playlist_new_name

        db.session.commit()

def get_songs_in_playlist_by_id(playlist_id):
    """
    Get all songs that are in playlist given by playlist id
    :param playlist_id:
    :return:
    """

    b = db.session.query(Track).filter(Track.track_id.in_(
        db.session.query(ComposePlaylistTrack.compose_playlist_track_track_id).filter(
            ComposePlaylistTrack.compose_playlist_track_playlist_id == playlist_id)
    )).all()
    print(b)
    return b


def duration_to_minutes(duree):
    duree = int(duree) // 1000

    minutes = round(duree // 60)
    seconds = round(duree - (minutes * 60))

    if seconds < 10:
        seconds = "0" + str(seconds)

    seconds = str(seconds)
    minutes = str(minutes)

    return f"{minutes}:{seconds}"


def minutes_to_duration(duree):
    x = duree.split(':')

    seconds = int(x[1])
    seconds += int(x[0]) * 60
    seconds = seconds * 1000

    return int(seconds)


def artist_to_dico(liste):
    artist_formated = []
    for artist in liste:
        dico = {
            "id": artist[0].artist_id,
            "name": artist[0].artist_name,
            "image": artist[0].artist_img
        }
        artist_formated.append(dico)

    return artist_formated


def album_to_dico(liste):
    albums_formated = []
    for album in liste:
        dico = {
            "couverture": album[0].album_cover_img,
            "titre": album[0].album_name,
            "artist": album[0].album_artist,
            "date": album[0].album_release_date,
            "description": album[0].album_description_fr,
            "id_album": album[0].album_id,
            "lien_album": "#",
            "modal_add_playlist_activator": "#",
            "modal_info_activator": "#"
        }
        albums_formated.append(dico)

    return albums_formated
