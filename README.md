# Project Flask rockDB

  

  <img  width="50%"  src="https://imgur.com/GiUd68X.png"/>

## Features 🚀 :

| Name | Description |
|--|--|
| **SignUp** | Create an account. Username , email, and password |
| **SignIn** | Connect to Spoticy using username and password |
| **Search Artist**| Create an account. Username , password |
| **Search Album**| Search an album and see his tracks. |
| **Edit Album**| Edit Album by changing his name and cover.|
| **Search Song**| Search a song and listen it. The song is linked to a youtube player. |
| **Search Gender**| Search a music gender. |
| **Search Artist**| Search an artist and edit it. |
| **Edit Artist**| Change artist picture and name. |
| **Create Playlist**| Add tracks into a playlist. |
| **Edit a Playlist**| Remove/Add/Edit tracks, playlist name and picture. |
| **Load Data**| Import default Json data into database. |
| **Create Track**| Create a song and add it to database. |
| **Create Album**| Create an Album and add it to database. |
| **Create Artist**| Create an artist and add it to database. |




## Run ⚡️
In **site** folder run : 

    python3 -m venv env
    source env/bin/activate
    cd spotify
    pip install -r requirements.txt 

Import data with `flask loaddb` . Start the server with `flask run --host="0.0.0.0"`.

Now open your browser and go to [http://localhost:5000](http://localhost:5000)     
**Please** be sure that you go on **localhost** and not ~~127.0.0.1~~. The youtube api doesn't allow local embed video, it requiere a domain name. 
  
  
## Development 🔨 :

Init a python virtual env.    
Go to spotify folder and run `flask run --host="0.0.0.0"`    

### How to enable flask profiler ? 
  In app.py set as True 

     app.config['DEBUG_TB_PROFILER_ENABLED'] = True

### How to intercept redirect ? 
 In app.py set as True 

    app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = True # by default False because intercept redirect is a debug feature only
    
### How to show flask debugger ? 
In app.py comment or uncomment this line
 `toolbar = DebugToolbarExtension(app)`

## Data :

  
All the music has been added using 2 api :

- [Musicbrainz](https://python-musicbrainzngs.readthedocs.io/en/latest/api/)

- [Theaudiodb](https://theaudiodb.com/)

To fetch data faster we run locally to avoid a ban from musicbrainz servers.

So with docker we installed the database server.

https://github.com/metabrainz/musicbrainz-docker

## Screenshots :
<div align="center">

### Player 
<img width="80%" src="https://imgur.com/KDQJL5a.png"/>       

### Playlists
<img width="80%" src="https://imgur.com/eJXpX9T.png"/>       

### Playlists details
<img width="80%" src="https://imgur.com/1oneuRP.png"/>       


### Album 
<img width="80%" src="https://imgur.com/dBpPwEN.png"/>      


### Home
<img  width="80%"  src="https://imgur.com/qikM6WK.png"/>       
</div>

